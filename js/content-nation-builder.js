console.log('You are now on Nation Builder!');

PathContainer = function () {
  this.html = document.querySelector('.content-area .signup-paths-container');
  let isVisible = this.getVisibility();

  isVisible.then(visibility => {
    this.html.classList.remove('hidden')
    this.visible = visibility;
  }).catch(visibility =>{
    this.html.classList.add('hidden');
    this.visible = visibility;
  });
};

PathContainer.prototype.getVisibility = function(){
  return new Promise((resolve, reject) => {
    chrome.storage.sync.get(['pathsVisible'], function(response){
      let visibility = response.pathsVisible;
      
      // If attribute not set, then paths are visible by default.
      // If [1, undefined] then it should be visible, otherwise not.
      if (typeof visibility === 'undefined') {
        resolve(1);
      } else {
        if (visibility == 1) resolve(1);
        if (visibility == 0) reject(0);
      }; 
    });
  });
};

PathContainer.prototype.setVisibility = function(value){
  return new Promise((resolve, reject) => {
    chrome.storage.sync.set({'pathsVisible': value}, function(){
      resolve();
    });
  });
};

PathContainer.prototype.show = function(){
  this.html.classList.remove('hidden');
  this.visible = 1;
};

PathContainer.prototype.hide = function(){
  this.html.classList.add('hidden');
  this.visible = 0;
};

PathContainer.prototype.visible = 0;

var pathContainer = new PathContainer();

ProfileContainer = function () {
  // Select the avatar column to add the path show/hide button.
  var avatarColumn = document.querySelector('.content-area .col-auto');
  // Change styling to accommodate the new button.
  avatarColumn.style.display = 'flex';
  avatarColumn.style.flexDirection = 'column';
  avatarColumn.style.justifyContent = 'space-between';

  // Generate the new button.
  var pathToggleButton = document.createElement('button');
  pathToggleButton.className = 'btn btn-mini mb-2 mr-1 text-nowrap';
  pathToggleButton.style.width = '90px';

  // Check if the paths are visible to adapt button text.
  var visibility = pathContainer.getVisibility();
  visibility.then(() => {
    pathToggleButton.innerHTML = '<span>Hide</span> paths';
  }).catch(() => {
    pathToggleButton.innerHTML = '<span>Show</span> paths';
  });

  // Hide/show paths container on click.
  pathToggleButton.onclick = function(){
    // Target button text
    const buttonText = pathToggleButton.querySelector('span');
    
    if (pathContainer.visible) {
      pathContainer.setVisibility(0).then(()=>{
        buttonText.innerHTML = 'Show';
        pathContainer.hide();
      });
    } else {
      pathContainer.setVisibility(1).then(()=>{
        buttonText.innerHTML = 'Hide';
        pathContainer.show();
      });
    };
  };

  // Add the newly created button to the avatar column.
  avatarColumn.appendChild(pathToggleButton);
};

var profileContainer = new ProfileContainer();

// chrome.runtime.sendMessage({action: 'togglePaths'});