Navigator = function () {
  this.navButtons = document.getElementsByClassName('btn-category');
  this.contentPanels = document.getElementById('content-panel')
    .getElementsByClassName('panel');
  this.init();

  this.panels = [];
  this.panels.push(new MembershipPanel());
}

Navigator.prototype.init = function () {
  this.bindButtons();
};

// 
Navigator.prototype.bindButtons = function (defaultFocus) {
  var navigator = this;
  var navButtons = [...this.navButtons];
  var contentPanels = [...this.contentPanels];

  // Bind button click actions
  navButtons.forEach(button => {
    button.onclick = function () {

      var foundPanel = contentPanels.find(panel => 
        this.dataset.activates == panel.dataset.panelName);
      
      if (!!foundPanel) {
        if (!!navigator.activePanel) {
          navigator.activePanel.classList.remove('active');
        };
        foundPanel.classList.toggle('active');
        navigator.activePanel = foundPanel;

        // See if we need to focus an element in particular.
        var focusElement = navigator.defaultFocus.find(focusElement =>
          foundPanel.dataset.panelName == focusElement.panelName
        );

        // Perform focus on found element.
        if (!!focusElement) {
          document.querySelector(focusElement.selector).focus();
        }
      };
    };
  });

};

// Start with no panels active.
Navigator.prototype.activePanel = undefined;

// Array which contains objects used to tell the Navigator what elements need
// to be focused on particular panels.
Navigator.prototype.defaultFocus = [
  {panelName: 'panel-profile', selector: '#member-id'}
];


MembershipPanel = function () {
  // Shortcuts
  membershipPanel = this;
  this.htmlElement = document.getElementById('membership-buttons');
  this.navButtons = [...document.querySelectorAll('#membership-buttons button')];
  
  // Target the ID field as we'll need it to construct de member's NB URL.
  var idField = document.getElementById('member-id');


  // Iterate nav buttons and target them to correct url.
  this.navButtons.forEach(button => {
    button.onclick = function () {
      var destionationUrl = 
        membershipPanel.generateUrl(idField.value, '#' + button.id);
      
      chrome.tabs.create({url: destionationUrl, active: false});
    };
  });
};

MembershipPanel.prototype.navButtons = [];

// Returns the destination url, depending on the navigation button which was
// clicked somewhere in the extension.
MembershipPanel.prototype.generateUrl = function (memberId, buttonSelector) {
  const baseUrl = 'https://impreuna.nationbuilder.com/admin';
  // All navigation buttons need to be added to the next array, regardless
  // of where they are in the app.
  const destinations = [
    {
      selector: '#btn-profile', 
      destinationUrl: baseUrl + '/signups/' + memberId
    },
    {
      selector: '#btn-membership', 
      destinationUrl: baseUrl + '/signups/' + memberId + '/memberships'
    },
    {
      selector: '#btn-custom-fields',
      destinationUrl: baseUrl + '/signups/' + memberId + '/custom_values'
    }];

  var resultUrl = destinations.find(
    destination => buttonSelector == destination.selector);

  return resultUrl.destinationUrl;
};

var navigator = new Navigator();