// Setup context menu object required to create menu button.
var contextMenuItem = {
  id: "plusMagnetizare",
  title: "Vezi profil",
  contexts: ["selection"]
};

// Create actual menu button based on above object.
chrome.contextMenus.create(contextMenuItem);

// Redirect users to the respective member's NB profile when clicked.
chrome.contextMenus.onClicked.addListener(function(clickData){
  // Only trigger redirection if text was selected.
  if (clickData.menuItemId == "plusMagnetizare" && !!clickData.selectionText) {
    // Get all integers in selection
    var integerPattern = /\d+/g;
    var userIds = clickData.selectionText.match(integerPattern);

    // Loop through all selected ids and open new tabs with their profiles.
    userIds.forEach(id => {
      // Generate correct URL. 
      const destionationUrl = 
        'https://impreuna.nationbuilder.com/admin/signups/' + id;

      // Perform redirection. 
      chrome.tabs.create({url: destionationUrl, active: false});
    });
  };
});
